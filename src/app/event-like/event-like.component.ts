import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from 'src/app/user-service.service';
@Component({
  selector: 'app-event-like',
  templateUrl: './event-like.component.html',
  styleUrls: ['./event-like.component.css']
})
export class EventLikeComponent implements OnInit {
  baseurl:any; user_id:any
  getfavrioutbyclientID:any; Apires:any; favitems:any
  deletefavoriteApi:any; deletefavoriteApires:any
  constructor(private http: HttpClient, private user: UserServiceService) { 
    this.user_id = localStorage.getItem('main_userid')
    this.baseurl = user.baseapiurl2
    this.getfavrioutbyclientID = this.baseurl + "api/event/getfavrioutbyclientID/"+this.user_id
    this.deletefavoriteApi = this.baseurl + "api/event/deletefavorite"
  }

  ngOnInit(): void {
    console.log(this.getfavrioutbyclientID)
    this.http.get(this.getfavrioutbyclientID).subscribe(res=>{
      this.Apires = res
      if(this.Apires.status){
        this.favitems = this.Apires.data
      }
    })
  }
  removeFav(id:any){
    console.log(id)
    const params={
      "user_id": this.user_id ,
      "event_id":id
    }
    this.http.post(this.deletefavoriteApi, params).subscribe(res=>{
      this.deletefavoriteApires = res
      if(this.deletefavoriteApires.status){
        window.location.reload()
      }
    })
  }

}
