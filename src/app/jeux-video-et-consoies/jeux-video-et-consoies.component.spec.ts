import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JeuxVideoEtConsoiesComponent } from './jeux-video-et-consoies.component';

describe('JeuxVideoEtConsoiesComponent', () => {
  let component: JeuxVideoEtConsoiesComponent;
  let fixture: ComponentFixture<JeuxVideoEtConsoiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JeuxVideoEtConsoiesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JeuxVideoEtConsoiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
