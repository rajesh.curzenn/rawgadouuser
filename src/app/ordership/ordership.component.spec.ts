import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdershipComponent } from './ordership.component';

describe('OrdershipComponent', () => {
  let component: OrdershipComponent;
  let fixture: ComponentFixture<OrdershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdershipComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
