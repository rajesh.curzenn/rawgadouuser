import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddNameComponent } from '../add-name/add-name.component';
import { AddPhoneComponent } from '../add-phone/add-phone.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {
  baseurl:any; user_id:any
  getuserprofile:any; getuserprofileRes:any
  firstname:any; 
  lastname:any
  email:any
  phone:any
  countryCode:any
  first_name :any
  last_name:any
  last_login_time:any;
  constructor(private seller: UserServiceService, private http: HttpClient,
  private router: Router, private activateroute: ActivatedRoute,private dialog: MatDialog,) { 
    this.baseurl = seller.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    let front_url = seller.front_url;
    if(this.user_id === "" || this.user_id == null   || this.user_id == undefined )
    {
      window.location.href = front_url;
    }

    this.last_login_time = localStorage.getItem('last_login_time');
    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = seller.front_url+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
         localStorage.clear();
         window.location.href = seller.front_url+"login";
      }
    }




    this.getuserprofile = this.baseurl + "api/user/getuserprofile/"+ this.user_id
    this.first_name = this.firstname
    this.last_name = ''
    this.countryCode = ''
  }

  ngOnInit(): void {
    this.http.get( this.getuserprofile).subscribe(res=>{
      this.getuserprofileRes= res
      console.log(this.getuserprofileRes)
      if(this.getuserprofileRes.status){
        console.log(this.getuserprofileRes)
        this.firstname = this.getuserprofileRes.data.first_name
        this.lastname = this.getuserprofileRes.data.last_name
        this.email = this.getuserprofileRes.data.email
        this.phone = this.getuserprofileRes.data.phone
        this.countryCode = this.getuserprofileRes.data.country_code
      }
    })
  }

  openName() {
    this.dialog.open(AddNameComponent,{
      data: { 
        "first_name" : this.firstname,
        "last_name": this.lastname

      }
    });
  }
  openPhone() {
    this.dialog.open(AddPhoneComponent,{
      data: { 
        "phone" : this.phone,
        "countryCode": this.countryCode

      }
    });

  }

  openPassword() {
    const dialogRef = this.dialog.open(ChangePasswordComponent);

    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  // callBack(name: string) {
  //   this.first_name = name
  // }
  

}
