import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { LoactionComponent } from '../loaction/loaction.component';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logopath = './assets/logo.png';
  products: any;
  itemlength: number=0;
  countries: any
  allSearchProducts: any; myoptions: any; baseurl: any; navURL: any
  allSearchProductsData: any; Querystring: string="";
  keyword = 'title';
  searchStringValue:string="";
  cartURL: any; user_id: any; gettedItems: any; cartitems: any
  cartOptions: any
  categoryURL: any
  parentCat: any
  childCatURL: any; childcat: any
  mySuggestion:any
  autocompleteOption:any; autocompleteURl:any
  mysuggestioncat:any
  allParentcat:any
  hide:any
  getuserprofile:any; getuserprofileRes:any
  username:any; userPic:any;
  myrating:any; condition:any; minval:number=0;maxval:number=0;
  redirectionUrl:string="";categoryURLParent:string="";allFeaturecat:any
  apiSent:number=0;
  notFoundText:any
  constructor(
    private dialog: MatDialog, private http: HttpClient, private user: UserServiceService,
    private router: Router, private route: ActivatedRoute) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')

    this.navURL = this.baseurl + "api/product/getallproducts"
    this.cartURL = this.baseurl + "api/cart/getcartbyuseridCopy"
    this.categoryURLParent = this.baseurl + "api/product/getOnlyParentCategory"
    this.categoryURL = this.baseurl + "api/product/getOnlyFeatureCategory"
    this.childCatURL = this.baseurl + "api/product/getOnlyChildCategory/"
    this.autocompleteURl = this.baseurl + "api/product/autoSuggestionProductList"
    this.getuserprofile = this.baseurl + "api/user/getuserprofile/"+ this.user_id

    this.products = [];
    this.allSearchProducts = []
    this.allSearchProductsData = []
    this.childcat = []
    this.mysuggestioncat = ''
    this.myoptions = {
      catagory: "",
      singleid: "",
      sort: 1,
      pricerangeLow: "",
      pricerangeHigh: "",
      rating: "",
      condition: ""
    }
    this.cartOptions = {
      userid: this.user_id
    }
    this.hide = false
    this.username = ''
    this.userPic = ''
    this.notFoundText = 'Pas de résultats'

  }

  ngOnInit(): void {
    this.getParentCategory();
    this.getFetureCategory();
    console.log(this.user_id)
    if (this.user_id != null) {
      this.http.post(this.cartURL, this.cartOptions).subscribe(res => {
        this.gettedItems = res
        this.cartitems = this.gettedItems.data
        // console.log("cart",this.cartitems )
        this.itemlength = this.cartitems.cart.length

      })

      this.http.get(this.getuserprofile).subscribe(res=>{
        this.getuserprofileRes = res
        if(this.getuserprofileRes.status){
          this.username = this.getuserprofileRes.data.first_name
          this.userPic = this.getuserprofileRes.data.photo
          console.log( this.getuserprofileRes.data)
        }
      })
    }
    if(this.route.snapshot.queryParams['query'] != "" && this.route.snapshot.queryParams['query'] != null)
    {
      this.Querystring = this.route.snapshot.queryParams['query'];
    }
    console.log("this.Querystring ",      this.Querystring);
  }




  getFetureCategory() {
    this.http.get(this.categoryURL).subscribe(res => {
      this.parentCat = res
      this.allFeaturecat = this.parentCat.data

     
    })
  }

  getParentCategory() {
    this.http.get(this.categoryURLParent).subscribe(res => {
      this.parentCat = res
      this.allParentcat = this.parentCat.data

     
    })
  }
  
  logout() {
    localStorage.clear();
    window.location.href = '/login'
  }

  openDialog() {
    const dialogRef = this.dialog.open(LoactionComponent);

    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  

  onChangeSearch(search: string) {
    console.log("on change event");
    this.Querystring = search;
    this.searchStringValue = search;
  }

  onFocused(e: any) {
    // console.log(e)
  }
  selectEvent(item: any) {
    this.Querystring = item.title;

    this.searchStringValue = item.title;
    

    console.log("select event ");
    console.log("item.title "+item.title);
    console.log("Querystring ", this.Querystring);
    //window.location.href = '/filter/' + item.title
  }

  filterData() {
    //window.location.href = '/filter/' + this.Querystring
    console.log("Querystring ", this.Querystring);

    console.log("searchStringValue ", this.searchStringValue);
    
    //return false;
    var aaaa = "";
    console.log("this.mysuggestioncat ",this.mysuggestioncat);
    if(!this.Querystring)
    {
      this.Querystring = "";
    }else{
      //aaaa = this.Querystring.title;
    }
    if(!this.searchStringValue)
    {
      this.searchStringValue = "";
    }
    this.redirectionUrl = "/"+"productList?cat_id="+this.mysuggestioncat+"&query="+this.searchStringValue;
    console.log("redirectionUrl ", this.redirectionUrl);
    window.location.href = this.redirectionUrl;

  }
  getvalCategory(eve: any) {
    this.mysuggestioncat = eve.target.value
    console.log(eve.target.value, "myid")
    //  window.location.href='/filter/'+eve.target.value
  }

  getSubcat(catid: any) {
    this.http.get(this.childCatURL + catid).subscribe(res => {
      this.childcat = res
      console.log(this.childcat.data)
    })

  }
  navigateCategory(child: any) {
    console.log(child)
    window.location.href = "/productList?cat_id=" + child
    //this.router.navigate(["/productList?cat_id="])+ child
  }

  onKeydownEvent(eve:any){

    //console.log(eve.target.value)
    if(this.apiSent == 0)
    {
      //console.log("hereee");
      this.apiSent = 1;
      this.autocompleteOption={
        "category_id":this.mysuggestioncat,
        "product_name":eve.target.value
       }
       
       this.http.post(this.autocompleteURl, this.autocompleteOption).subscribe(res=>{
        
        //console.log("auto suggestion ", res);

        this.apiSent = 0;
          this.mySuggestion = res;
          this.allSearchProductsData = this.mySuggestion.data
   
       })
    }
    
   }
   display(){
    this.hide = true
   }
  }




