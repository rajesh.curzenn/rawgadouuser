import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { UserChatService } from '../user-chat.service';
@Component({
  selector: 'app-messeges-chat',
  templateUrl: './messeges-chat.component.html',
  styleUrls: ['./messeges-chat.component.css']
})
export class MessegesChatComponent implements OnInit {
  baseurl: any; user_id: any
  get_all_seller_chat: any; sellerRes: any; sellerChat: any
  get_all_message_room_user: any; chatRes: any; myChat: any
  userPhoto: any; productImg: any; productName: any; productDesc: any
  chatform!: FormGroup

  delete_chat_seller_by_room_id:any
  delete_chat_seller_by_room_idRes:any

  productId: any; sellerId:any;  last_login_time:any;
  constructor(private user: UserServiceService, private http: HttpClient,
    private chatservice:UserChatService) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')

    let front_url = user.front_url;
    if(this.user_id === "" || this.user_id == null   || this.user_id == undefined )
    {
      window.location.href = front_url;
    }

    this.last_login_time = localStorage.getItem('last_login_time');
    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = user.front_url+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
         localStorage.clear();
         window.location.href = user.front_url+"login";
      }
    }




    this.get_all_seller_chat = this.baseurl + "api/chat/get_all_user_chat/" + this.user_id
    this.get_all_message_room_user = this.baseurl + "api/chat/get_all_message_room_user/"

    this.delete_chat_seller_by_room_id = this.baseurl +"api/chat/delete_chat_user_by_room_id/"

    this.myChat = []
    this.userPhoto = ''
    this.productImg = ''
    this.productName = ''
    this.productDesc = ''
    this.productId = ''
    this.sellerId =''

    this.chatform = new FormGroup({
      message: new FormControl(''),
     
    })

  }

  ngOnInit(): void {
    this.getsellerChat()

    this.chatservice.getNewMessage().subscribe((response: any) => {
      console.log(response)
     if(response.status)
     { 
      this.chatform.reset();
       this.myChat = response.chat;
       const assenData = this.myChat.sort((a: { created_at: string | number | Date; }, b: { created_at: string | number | Date; }) => {
        return <any>new Date(a.created_at) - <any>new Date(b.created_at);
      });
     }
      
   })
  }
  getsellerChat() {

    this.http.get(this.get_all_seller_chat).subscribe(res => {
      this.sellerRes = res
      console.log(" this.sellerRes ",  this.sellerRes);
      if (this.sellerRes.status) {
        this.sellerChat = this.sellerRes.data
        
        if(this.sellerChat.length > 0){
          this.openChat(this.sellerChat[0]._id, this.sellerChat[0].pro_rec_images[0][0], this.sellerChat[0])
        }
      
      }
    })

  }
  openChat(roomId: any, img: any, seller: any) {

    this.chatform.reset();
    this.http.get(this.get_all_message_room_user + roomId).subscribe(res => {
      this.chatRes = res
      if (this.chatRes.status) {
        this.myChat = this.chatRes.data

        this.userPhoto = this.myChat[0].photo
        this.productImg = img
        this.productName = seller.pro_rec_title
        this.productDesc = seller.pro_rec_title
        this.productId = seller.product_id
        this.sellerId = seller.seller_id
        
       // console.log(this.myChat[0].photo)
        const assenData = this.myChat.sort((a: { created_at: string | number | Date; }, b: { created_at: string | number | Date; }) => {
          return <any>new Date(a.created_at) - <any>new Date(b.created_at);
        });
      }
    })

  }
  sendmsg() {

    console.log("submittttttttttttttttt ");
    
    const params = {
      "product_id":this.productId,
      "seller_id":this.sellerId,
      "user_id":this.user_id,
      "message":this.chatform.value.message,
      "message_by":2
    }
   

    this.chatservice.sendMessage(params)
   
  }


  deletemyChat(mychat:any){
    
    const deleteId= mychat._id
    const conf = confirm("Voulez-vous supprimer ce chat ?")
    if(conf){
      this.http.get(this.delete_chat_seller_by_room_id+deleteId).subscribe(res=>{
        this.delete_chat_seller_by_room_idRes = res
        if(this.delete_chat_seller_by_room_idRes.status){
          window.location.reload()
        }
      })
    }
    
  }
}
