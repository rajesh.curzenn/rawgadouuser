import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
  baseurl:any; mainid:any; apiResponse:any; front_url:any;
  last_login_time:any;
  constructor( private userService:UserServiceService, private http: HttpClient)
  {
    this.mainid = localStorage.getItem('main_userid')
    this.baseurl = userService.baseapiurl2;
    this.front_url = userService.front_url;
    
    this.last_login_time = localStorage.getItem('last_login_time');


    console.log(" this.mainid " , this.mainid);
    if(this.mainid == "" || this.mainid == null || this.mainid == undefined ||  this.mainid == 'null' || this.mainid == 'undefined')
    {
      window.location.href = userService.front_url+"login";
    }

    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = userService.front_url+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
        localStorage.clear();
        window.location.href = userService.front_url+"login";
      }
    }
  }

  ngOnInit(): void {
  }

  deleteAc()
  {
    console.log(" this.mainid " , this.mainid);
    var result = confirm("Vouloir supprimer ?");
    if (result) {
      this.http.get(this.baseurl+"api/user/deleteUserAc/"+this.mainid ).subscribe(res => { 
        console.log(" res ", res);
        this.apiResponse = res;
        if(this.apiResponse.status == true)
        {
          console.log("hereeee AC delete");
           localStorage.clear();
          window.location.href= this.front_url+'/login'
        }
        
      })
    }
  }
}
