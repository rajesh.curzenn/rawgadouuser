import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css']
})
export class ViewOrderComponent implements OnInit {
  part_id: any; table_id: any; baseurl: any
  get_order_detail_by_part_id: any; get_order_detail_by_part_idRes: any
  singleOrder: any
  constructor(private seller: UserServiceService, private http: HttpClient, private activateroute: ActivatedRoute) { 
    this.baseurl = seller.baseapiurl2
    this.activateroute.paramMap.subscribe(params => {
      this.part_id = params.get('id');
      this.table_id = params.get('id1');

    });
    this.get_order_detail_by_part_id = this.baseurl + "api/order/get_order_detail_by_part_id"

  }

  ngOnInit(): void {
    const parms = {
      "id": this.part_id,
      "part_id": this.table_id
    }

    this.http.post(this.get_order_detail_by_part_id, parms).subscribe(res => {
      this.get_order_detail_by_part_idRes = res
      if (this.get_order_detail_by_part_idRes.status) {
        this.singleOrder = this.get_order_detail_by_part_idRes.all_info
        console.log( this.singleOrder)
       

       
      }
    })
  }

}
