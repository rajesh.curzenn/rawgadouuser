import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-loaction',
  templateUrl: './loaction.component.html',
  styleUrls: ['./loaction.component.css']
})
export class LoactionComponent implements OnInit {

  constructor( private dialog: MatDialog) { }

  ngOnInit(): void {
  }
  close(){
    this.dialog.closeAll();
  }

}
