import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-event-bootcamp',
  templateUrl: './event-bootcamp.component.html',
  styleUrls: ['./event-bootcamp.component.css']
})
export class EventBootcampComponent implements OnInit {
  baseurl: any; orderid:any; userid:any
  getsinglebookingbyid:any; Apires:any; ticket:any
  ticketsArr:any
  downloadinvoice:any; downloadinvoiceRes:any
  downloadtickets:any; TicketRes:any
  constructor(private seller: UserServiceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.orderid = this.activateroute.snapshot.params['id']
      this.userid = localStorage.getItem('main_userid')

      this.getsinglebookingbyid = this.baseurl+"api/event/getsinglebookingbyid/"+this.orderid
      this.downloadinvoice = this.baseurl+"api/event/downloadinvoice/"+this.orderid
      this.downloadtickets = this.baseurl+"api/event/downloadtickets/"+this.orderid
     }

  ngOnInit(): void {
     this.http.get(this.getsinglebookingbyid).subscribe(res=>{
      this.Apires = res
      if(this.Apires.status){
        this.ticket = this.Apires.data
        this.ticketsArr = this.ticket.tickets

        console.log(this.ticket)
        console.log(this.ticketsArr)
      }
     })

  }
  downloadInvoice(){

    this.http.get(this.downloadinvoice).subscribe(res=>{
      this.downloadinvoiceRes = res
      if(this.downloadinvoiceRes.status){
        window.open(this.downloadinvoiceRes.url, '_target');
      }
    })
    
  }
  downloadTicket(){
    this.http.get( this.downloadtickets ).subscribe(res=>{
      this.TicketRes = res
      if(this.TicketRes.status){
        const ticket = this.TicketRes.ticketfiles.length
        const arr: any[] = []
        for(var i = 0; i < ticket; i++){
          
          arr.push(this.baseurl+"static/public/invoices/"+this.TicketRes.ticketfiles[i])
         
        }
        
        
        arr.forEach(url => {
          window.open(url, '_target')
      })
        
      
      }
    })
  }

}
