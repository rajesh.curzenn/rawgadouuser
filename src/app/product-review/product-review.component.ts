import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms'; 

@Component({
  selector: 'app-product-review',
  templateUrl: './product-review.component.html',
  styleUrls: ['./product-review.component.css']
})
export class ProductReviewComponent implements OnInit {
  part_id: any; table_id: any; baseurl: any
  get_order_detail_by_part_id: any; get_order_detail_by_part_idRes: any
  singleOrder: any 
  userid:any; providedid:any; productid:any
  myrating:any
  createrating:any; createratingRes:any

  reviewform!:FormGroup
  constructor(private seller: UserServiceService, private http: HttpClient, private activateroute: ActivatedRoute,private router:Router) {
    this.baseurl = seller.baseapiurl2
    this.userid = localStorage.getItem('main_userid')
    this.activateroute.paramMap.subscribe(params => {
      this.part_id = params.get('id');
      this.table_id = params.get('id1');

    });
    this.get_order_detail_by_part_id = this.baseurl + "api/order/get_order_detail_by_part_id"
    this.createrating = this.baseurl + "api/order/createrating"


    this.reviewform = new FormGroup({  
      review: new FormControl(''),
    })
    this.myrating = ''
   }

  ngOnInit(): void {
    const parms = {
      "id": this.part_id,
      "part_id": this.table_id
    }

    this.http.post(this.get_order_detail_by_part_id, parms).subscribe(res => {
      this.get_order_detail_by_part_idRes = res
      if (this.get_order_detail_by_part_idRes.status) {
        this.singleOrder = this.get_order_detail_by_part_idRes.all_info
        this.providedid= this.get_order_detail_by_part_idRes.data[0].products[0].product.provider_id
        this.productid= this.get_order_detail_by_part_idRes.data[0].products[0].product._id

        console.log(this.providedid,this.productid)
       
      }
    })
  }
  setRating(event:any){
    this.myrating = event.target.value
    console.log(event.target.value)
  }
  
  send(){


     this.reviewform.value.user_id =this.userid,
     this.reviewform.value.provider_id=this.providedid,
     this.reviewform.value.product_id =this.productid,
     this.reviewform.value.order_id=this.part_id,
     this.reviewform.value.rating = this.myrating

     console.log(this.createrating ,this.reviewform.value)
    if(!this.reviewform.value.rating)
    {
      this.createratingRes = {status:false,message:"Le classement est requis"};
      return;
    }
    if(!this.reviewform.value.user_id)
    {

      this.createratingRes = {status:false,message:"L'identifiant de l'utilisateur est requis"};
      return;
    }
    if(!this.reviewform.value.provider_id)
    {

      this.createratingRes = {status:false,message:"L'identifiant du prestataire est requis"};
      return;
    }
    if(!this.reviewform.value.product_id)
    {

      this.createratingRes = {status:false,message:"L'identifiant du produit est requis"};
      return;
    }
    if(!this.reviewform.value.order_id)
    {

      this.createratingRes = {status:false,message:"L'identifiant de la commande est requis"};
      return;
    }
     
     this.http.post(this.createrating ,this.reviewform.value).subscribe(res=>{
      this.createratingRes =  res;
      if(this.createratingRes.status == true)
      {
        setTimeout(()=>{
          //window.location.reload();
          //product_order
          this.router.navigate(["/product_order"]);
        },2000);
      }
      console.log( "dfhsdf")
     })
  }
}
