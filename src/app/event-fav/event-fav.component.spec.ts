import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventFavComponent } from './event-fav.component';

describe('EventFavComponent', () => {
  let component: EventFavComponent;
  let fixture: ComponentFixture<EventFavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventFavComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EventFavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
